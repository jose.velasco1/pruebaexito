#language: es
Característica: agregar al carrito
  Yo como cliente del Éxito
  deseo agregar productos al carrito de la página
  para poder realizar compras en línea

  @LoginCorrecto
  Escenario: login correcto
    Dado que como cliente he ingresado a la homepage del aplicativo
    Cuando ingrese mis credenciales validas
      | email    | prueba.exito@mailsac.com |
      | contraseña | *.Ex1t0*                 |
    Entonces podre ingresar hasta mis datos de perfil


  @AgregarVerduraAlCarrito
  Escenario: agregar verdura al carrito
    Dado que como cliente he ingresado al aplicativo
      | email    | prueba.exito@mailsac.com |
      | contraseña | *.Ex1t0*                 |
    Cuando agregue la lechuga de menor costo al carrito
    Y continue hasta la pantalla de pagar producto
    Entonces vere el mensaje de confirmacion "Finalizar la compra"
