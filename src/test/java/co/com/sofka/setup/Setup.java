package co.com.sofka.setup;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static co.com.sofka.utilities.Dictionary.WEB_URL;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class Setup {

    private static String actorName;
    protected WebDriver webDriver;
    private String SO = System.getProperty("os.name").toLowerCase();

    private void setupBrowser() {
        if (SO.contains("nux")) {
            WebDriverManager.chromedriver().linux().setup();
            //WebDriverManager.firefoxdriver().linux().setup();

        } else if (SO.contains("os x")) {
            WebDriverManager.chromedriver().mac().setup();
            //WebDriverManager.firefoxdriver().mac().setup();
        } else {
            WebDriverManager.chromedriver().win().setup();
            //WebDriverManager.firefoxdriver().win().setup();
        }

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito", "--Start-maximized", "--disable-notifications", "--disable-plugins");
        this.webDriver = new ChromeDriver(options);
        this.webDriver.get(WEB_URL);
    }

    public Setup theActorNamed(String name){
        OnStage.setTheStage(new OnlineCast());
        this.actorName = name;
        return new Setup();
    }

    public void setupTheBrowser(){
        setupBrowser();
        theActorCalled(this.actorName).can(BrowseTheWeb.with(this.webDriver));
    }


}
