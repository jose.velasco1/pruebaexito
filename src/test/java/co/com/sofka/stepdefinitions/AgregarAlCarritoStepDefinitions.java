package co.com.sofka.stepdefinitions;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import java.util.Map;

public class AgregarAlCarritoStepDefinitions {

    @Dado("que he ingresado al aplicativo")
    public void queHeIngresadoAlAplicativo(Map<String,String> dataTable) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        throw new io.cucumber.java.PendingException();
    }
    @Cuando("agregue la lechuga de menor costo al carrito")
    public void agregueLaLechugaDeMenorCostoAlCarrito() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    @Cuando("continue hasta la pantalla de pagar producto")
    public void continueHastaLaPantallaDePagarProducto() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    @Entonces("vere el mensaje de confirmacion {string}")
    public void vereElMensajeDeConfirmacion(String mensajeDeConfirmacion) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
}
