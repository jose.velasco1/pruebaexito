package co.com.sofka.stepdefinitions;

import co.com.sofka.setup.Setup;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.ensure.Ensure;

import java.util.Map;

import static co.com.sofka.questions.VerMensajeBienvenida.theGreetingsMessage;
import static co.com.sofka.tasks.IrAPaginaPerfil.goToProfilePage;
import static co.com.sofka.tasks.Ingresar.loginToSite;
import static co.com.sofka.tasks.AbrirPaginaPrincipal.openTheHomePage;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.containsString;

public class LoginCorrectoStepDefinitions extends Setup {

    private static String JOSE = "José";

    @Dado("que como cliente he ingresado a la homepage del aplicativo")
    public void queComoClienteHeIngresadoALaHomepageDelAplicativo() {
        theActorNamed(JOSE).setupTheBrowser();
        theActorInTheSpotlight().wasAbleTo(openTheHomePage());
    }

    @Cuando("ingrese mis credenciales validas")
    public void ingreseMisCredencialesValidas(Map<String, String> dataTable) {

        theActorInTheSpotlight().attemptsTo(
                loginToSite()
                        .with(dataTable.get("email"))
                        .andWith(dataTable.get("contraseña"))
        );


       /*theActorInTheSpotlight().attemptsTo(
               Click.on(PageElement
                       .located(
                               By.cssSelector(".exito-header-3-x-headerLinksIconW"))),
               Click.on(PageElement
                       .located(
                               By.cssSelector(".vtex-login-2-x-optionsListItem:nth-child(2) .vtex-button__label"))),
               Enter.theValue(dataTable.get("email"))
                       .into(InputField.located(
                               By.xpath("//form/div/label/div/input"))),
               Enter.theValue(dataTable.get("email"))
                       .into(InputField.located(
                               By.xpath("//form/div/label/div/input"))),
               Click.on(Button.located(By.xpath("//div[2]/button/div")))
       );*/
    }

    @Entonces("podre ingresar hasta mis datos de perfil")
    public void podreIngresarHastaMisDatosDePerfil() {

        theActorInTheSpotlight().attemptsTo(
                goToProfilePage(),
                Ensure.that(theGreetingsMessage()).contains("Hola")
        );

        /*theActorInTheSpotlight().should(
                seeThat(
                        theGreetingsMessage(), containsString("Hola")
                )

        );*/

    }
}
