package co.com.sofka.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.interfaces.PaginaPerfil.perfilEmail;

public class VerInformacionPerfil implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return perfilEmail.resolveFor(actor).getTextContent();
    }

    public static VerInformacionPerfil theProfileInformation(){
        return new VerInformacionPerfil();
    }
}
