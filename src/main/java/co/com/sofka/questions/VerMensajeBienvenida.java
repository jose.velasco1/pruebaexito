package co.com.sofka.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.interfaces.PaginaPerfil.mensajeBienvenida;

public class VerMensajeBienvenida implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return mensajeBienvenida.resolveFor(actor).getText();
    }

    public static VerMensajeBienvenida theGreetingsMessage(){
        return new VerMensajeBienvenida();
    }
}
