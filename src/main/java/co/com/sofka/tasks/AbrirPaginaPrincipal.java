package co.com.sofka.tasks;

import co.com.sofka.interfaces.PaginaPrincipal;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirPaginaPrincipal implements Task {

    private PaginaPrincipal homePage;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(
                Open.browserOn(homePage)
        );
    }

    public static AbrirPaginaPrincipal openTheHomePage() {
        return new AbrirPaginaPrincipal();
    }

}
