package co.com.sofka.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.ui.Button;

import static co.com.sofka.interfaces.PaginaPerfil.enlacePerfil;


public class IrAPaginaPerfil implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(enlacePerfil)
        );
    }

    public static IrAPaginaPerfil goToProfilePage(){
        return new IrAPaginaPerfil();
    }
}
