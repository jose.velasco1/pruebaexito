package co.com.sofka.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.sofka.interfaces.PaginaPrincipal.enlaceMisPedidos;
import static co.com.sofka.interfaces.PaginaMisPedidos.*;


public class Ingresar implements Task {

    private String email;
    private String contrasena;

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(enlaceMisPedidos),
                Click.on(btnIngresarConEmailYContrasena),
                Enter.theValue(email).into(campoEmail),
                Enter.theValue(contrasena).into(campoContrasena),
                Click.on(btnEntrar)
        );
    }

    public Ingresar with(String email){
        this.email=email;
        return this; //Es importante usar this en el return
    }

    public Ingresar andWith(String contrasena){
        this.contrasena = contrasena;
        return this;
    }

    public static Ingresar loginToSite(){
        return new Ingresar();
    }
}
