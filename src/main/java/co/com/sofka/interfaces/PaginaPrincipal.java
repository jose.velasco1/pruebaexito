package co.com.sofka.interfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class PaginaPrincipal extends PageObject {

    /* public static final Target btnMiCuenta =
             Target.the("Boton Mi Cuenta Home").located(By.cssSelector(".exito-header-3-x-headerLinksIconW"));
 */
    /*public static final Target btnIngresarConEmailYContrasena =
            Target.the("Boton Ingresar con email y contraseña").located(By.cssSelector(".vtex-login-2-x-optionsListItem:nth-child(2) .vtex-button__label"));
*/
    /*public static final Target divCampoEmail =
            Target.the("Div campo email").located(By.xpath("//form/div/label/div"));

    public static final Target divCampoContrasena =
            Target.the("Div campo contraseña").located(By.xpath("//div/div/div/div/div/div/form/div[2]/div/label/div"));

    public static final Target campoEmail =
            Target.the("Campo de texto email").located(By.xpath("//form/div/label/div/input"));

    public static final Target campoContrasena =
            Target.the("Campo de texto contraseña").located(By.xpath("//div[2]/div/label/div/input"));

    public static final Target btnEntrar =
            Target.the("Boton entrar").located(By.xpath("//div[2]/button/div"));
*/
    public static final Target enlaceMisPedidos =
            Target.the("Enlace Mis Pedidos").located(By.cssSelector(".exito-header-3-x-menuButtonIcon:nth-child(1) > .exito-header-3-x-menuButtonIconContent"));

}