package co.com.sofka.interfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class PaginaPerfil extends PageObject {


    /*public static final Target enlaceMiCuenta =
            Target.the("Enlace Mi Cuenta").located(By.xpath(".exito-header-3-x-itemHeaderLinkW:nth-child(1) .underline-hover"));
*/

    public static final Target mensajeBienvenida =
            Target.the("Mensaje de bienvenida al perfil").located(By.cssSelector(".vtex-my-account-1-x-userGreeting"));

    public static final Target perfilEmail =
            Target.the("Correo del perfil").located(By.xpath("//main/div/div[2]/div"));

    public static final Target enlaceDirecciones =
            Target.the("Enlace a Direcciones").located(By.linkText("Direcciones"));

    public static final Target enlacePerfil =
            Target.the("Enlace a Perfil").located(By.linkText("Perfil"));

    public static final Target resumenDireccion =
            Target.the("Resumen de dirección").located(By.cssSelector(".address-summary"));

}
