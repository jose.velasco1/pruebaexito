package co.com.sofka.interfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class PaginaMisPedidos extends PageObject {

    public static final Target btnEntrar =
            Target.the("Boton Entrar, Mis Pedidos").located(By.cssSelector(".min-h-small > .vtex-button__label"));

    public static final Target campoEmail =
            Target.the("Campo Email, Mis Pedidos").located(By.cssSelector(".vtex-input-prefix__group > .ph5"));

    public static final Target campoContrasena =
            Target.the("Campo Contraseña, Mis Pedidos").located(By.cssSelector(".h-regular > .br-0"));

    public static final Target btnIngresarConEmailYContrasena =
            Target.the("Boton Ingresar con email y contraseña, Mis Pedidos").located(By.cssSelector(".ph6 > span:nth-child(1)"));
}
